import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  final bool isCircularLoader;
  final Color? loaderColor;

  const LoadingWidget({
    super.key,
    this.isCircularLoader = true,
    this.loaderColor,
  });

  @override
  Widget build(BuildContext context) {
    if (Platform.isIOS) {
      return const Center(
        child: CupertinoActivityIndicator(),
      );
    } else {
      return Center(
        child: isCircularLoader
            ? CircularProgressIndicator(
                color: loaderColor,
              )
            : LinearProgressIndicator(
                color: loaderColor,
              ),
      );
    }
  }
}
