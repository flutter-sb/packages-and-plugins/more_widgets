/// Copyright 2023, Simon Braillard
/// Simon Braillard - more_widgets
///
/// File : dialogs.dart
/// Author : Simon Braillard
/// Date : 10.03.23

import 'package:flutter/material.dart';
import 'package:more_widgets/src/dialogs/dialog_with_options.dart';
import 'package:more_widgets/src/dialogs/loading_dialog.dart';
import 'package:more_widgets/src/dialogs/text_input_dialog.dart';

import 'info_dialog.dart';

enum DestructiveAction { left, right, none }

enum DefaultAction { left, right, none }

/// This class is used to display dialogs easily without worrying about what
/// platform you use to display the dialog. It will automatically adapt to iOS
/// or other platforms.
abstract class Dialogs {
  /// This method will display an informative dialog with just one button
  static Future<void> infoDialog({
    required BuildContext context,
    required String title,
    required String message,
    bool barrierDismissible = true,

    /// Button text, default : Ok
    String buttonText = "Ok",
    bool popByDefault = true,
    Function? onPressed,
  }) async =>
      await showDialog(
          barrierDismissible: barrierDismissible,
          context: context,
          builder: (context) {
            if (Theme.of(context).platform == TargetPlatform.iOS) {
              return CupertinoInfoDialog(
                title: title,
                message: message,
                buttonText: buttonText,
                onPressed: onPressed,
                popByDefault: popByDefault,
              );
            }
            return MaterialInfoDialog(
              title: title,
              message: message,
              buttonText: buttonText,
              onPressed: onPressed,
              popByDefault: popByDefault,
            );
          });

  /// Here is the dialog with options, it's useful if you want to pop up a dialog where you can choose between two
  /// options. One of them can be set as destructive action.
  static Future<void> dialogWithOptions({
    required BuildContext context,
    required String title,
    required String message,
    bool barrierDismissible = true,
    String textLeftButton = 'OK',
    String textRightButton = 'Cancel',
    Function? onPressedLeftButton,
    Function? onPressedRightButton,
    DestructiveAction destructiveAction = DestructiveAction.right,
    DefaultAction defaultAction = DefaultAction.none,
    Color androidDestructiveColor = Colors.red,
    bool popByDefault = true,
  }) async =>
      await showDialog(
          context: context,
          barrierDismissible: barrierDismissible,
          builder: (context) {
            if (Theme.of(context).platform == TargetPlatform.iOS) {
              return CupertinoDialogWithOptions(
                title: title,
                message: message,
                textLeftButton: textLeftButton,
                textRightButton: textRightButton,
                onPressedLeftButton: onPressedLeftButton,
                onPressedRightButton: onPressedRightButton,
                destructiveAction: destructiveAction,
                defaultAction: defaultAction,
                popByDefault: popByDefault,
              );
            }
            return MaterialDialogWithOptions(
              title: title,
              message: message,
              textLeftButton: textLeftButton,
              textRightButton: textRightButton,
              onPressedLeftButton: onPressedLeftButton,
              onPressedRightButton: onPressedRightButton,
              destructiveAction: destructiveAction,
              defaultAction: defaultAction,
              popByDefault: popByDefault,
              androidDestructiveColor: androidDestructiveColor,
            );
          });

  /// This method will display a dialog with a loading indicator inside
  static Future<void> loadingDialog({
    required BuildContext context,
    bool barrierDismissible = false,
    String? title,
  }) async {
    await showDialog(
        barrierDismissible: barrierDismissible,
        context: context,
        builder: (context) {
          if (Theme.of(context).platform == TargetPlatform.iOS) {
            return CupertinoLoadingDialog(title: title);
          }
          return MaterialLoadingDialog(title: title);
        });
  }

  /// This method will display a dialog with two buttons
  /// that have actions
  static Future<void> textInputDialog({
    required BuildContext context,
    required String title,
    required String message,
    bool barrierDismissible = true,
    String textLeftButton = 'OK',
    String textRightButton = 'Cancel',
    Function? onPressedLeftButton,
    Function? onPressedRightButton,
    DestructiveAction destructiveAction = DestructiveAction.right,
    DefaultAction defaultAction = DefaultAction.none,
    Color androidDestructiveColor = Colors.red,
    bool popByDefault = true,
    bool hasSecondaryButton = true,
    String? placeholder,
    TextEditingController? controller,
    TextInputType? keyboardType,
    Function? onChanged,
    Function? onEditingComplete,
  }) async =>
      await showDialog(
          context: context,
          barrierDismissible: barrierDismissible,
          builder: (context) {
            if (Theme.of(context).platform == TargetPlatform.iOS) {
              return CupertinoTextInputDialog(
                message: message,
                title: title,
                defaultAction: defaultAction,
                destructiveAction: destructiveAction,
                textLeftButton: textLeftButton,
                textRightButton: textRightButton,
                onPressedLeftButton: onPressedLeftButton,
                onPressedRightButton: onPressedRightButton,
                popByDefault: popByDefault,
                hasSecondaryButton: hasSecondaryButton,
                placeholder: placeholder,
                controller: controller,
                keyboardType: keyboardType,
                onChanged: onChanged,
                onEditingComplete: onEditingComplete,
              );
            }

            return MaterialTextInputDialog(
              message: message,
              title: title,
              defaultAction: defaultAction,
              destructiveAction: destructiveAction,
              textLeftButton: textLeftButton,
              textRightButton: textRightButton,
              onPressedLeftButton: onPressedLeftButton,
              onPressedRightButton: onPressedRightButton,
              popByDefault: popByDefault,
              hasSecondaryButton: hasSecondaryButton,
              placeholder: placeholder,
              controller: controller,
              keyboardType: keyboardType,
              onChanged: onChanged,
              onEditingComplete: onEditingComplete,
              androidDestructiveColor: androidDestructiveColor,
            );
          });
}
