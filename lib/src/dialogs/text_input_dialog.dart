/// Copyright 2023, simonbraillard
/// simonbraillard - more_widgets
///
/// File : textInputDialog.dart
/// Author : simonbraillard
/// Date : 10.03.23

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:more_widgets/more_widgets.dart';

class CupertinoTextInputDialog extends StatelessWidget {
  final String title;
  final String message;

  final String textLeftButton;
  final String textRightButton;
  final Function? onPressedLeftButton;
  final Function? onPressedRightButton;

  final DestructiveAction destructiveAction;
  final DefaultAction defaultAction;
  final bool popByDefault;
  final bool hasSecondaryButton;

  final String? placeholder;
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final Function? onChanged;
  final Function? onEditingComplete;

  const CupertinoTextInputDialog(
      {required this.title,
      required this.message,
      required this.popByDefault,
      required this.destructiveAction,
      required this.defaultAction,
      required this.textLeftButton,
      required this.textRightButton,
      required this.hasSecondaryButton,
      this.controller,
      this.keyboardType,
      this.placeholder,
      this.onChanged,
      this.onEditingComplete,
      this.onPressedLeftButton,
      this.onPressedRightButton,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Column(
        children: [
          Text(message),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: CupertinoTextField(
              placeholder: placeholder,
              controller: controller,
              keyboardType: keyboardType,
              onChanged: (value) => onChanged?.call(value),
              onEditingComplete: () => onEditingComplete?.call(),
            ),
          )
        ],
      ),
      actions: [
        if (hasSecondaryButton)
          CupertinoDialogAction(
            isDestructiveAction:
                destructiveAction == DestructiveAction.left ? true : false,
            isDefaultAction: defaultAction == DefaultAction.left ? true : false,
            onPressed: () {
              if (popByDefault) Navigator.of(context).pop();
              onPressedLeftButton?.call();
            },
            child: Text(textLeftButton),
          ),
        CupertinoDialogAction(
          isDestructiveAction:
              destructiveAction == DestructiveAction.right ? true : false,
          isDefaultAction: defaultAction == DefaultAction.right ? true : false,
          onPressed: () {
            if (popByDefault) Navigator.of(context).pop();
            onPressedRightButton?.call();
          },
          child: Text(textRightButton),
        ),
      ],
    );
  }
}

class MaterialTextInputDialog extends StatelessWidget {
  final String title;
  final String message;

  final String textLeftButton;
  final String textRightButton;
  final Function? onPressedLeftButton;
  final Function? onPressedRightButton;

  final DestructiveAction destructiveAction;
  final DefaultAction defaultAction;
  final bool popByDefault;
  final bool hasSecondaryButton;
  final Color androidDestructiveColor;

  final String? placeholder;
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final Function? onChanged;
  final Function? onEditingComplete;

  const MaterialTextInputDialog(
      {required this.title,
      required this.message,
      required this.popByDefault,
      required this.destructiveAction,
      required this.defaultAction,
      required this.textLeftButton,
      required this.textRightButton,
      required this.hasSecondaryButton,
      required this.androidDestructiveColor,
      this.controller,
      this.keyboardType,
      this.placeholder,
      this.onChanged,
      this.onEditingComplete,
      this.onPressedLeftButton,
      this.onPressedRightButton,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(message),
          Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: TextField(
              controller: controller,
              keyboardType: keyboardType,
              onChanged: (value) => onChanged?.call(value),
              onEditingComplete: () => onEditingComplete?.call(),
              decoration: InputDecoration(
                hintText: placeholder,
              ),
            ),
          )
        ],
      ),
      actions: [
        if (hasSecondaryButton)
          TextButton(
            onPressed: () {
              if (popByDefault) Navigator.of(context).pop();
              onPressedLeftButton?.call();
            },
            child: Text(textLeftButton,
                style: TextStyle(
                  color: destructiveAction == DestructiveAction.left
                      ? androidDestructiveColor
                      : null,
                  fontWeight: defaultAction == DefaultAction.left
                      ? FontWeight.bold
                      : null,
                )),
          ),
        TextButton(
          onPressed: () {
            if (popByDefault) Navigator.of(context).pop();
            onPressedRightButton?.call();
          },
          child: Text(textRightButton,
              style: TextStyle(
                color: destructiveAction == DestructiveAction.right
                    ? androidDestructiveColor
                    : null,
                fontWeight: defaultAction == DefaultAction.right
                    ? FontWeight.bold
                    : null,
              )),
        ),
      ],
    );
  }
}
