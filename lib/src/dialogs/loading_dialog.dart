/// Copyright 2023, simonbraillard
/// simonbraillard - more_widgets
///
/// File : loading_dialog.dart
/// Author : simonbraillard
/// Date : 10.03.23

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CupertinoLoadingDialog extends StatelessWidget {
  final String? title;

  const CupertinoLoadingDialog({this.title, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: title == null ? null : Text(title!),
      content: Padding(
        padding: EdgeInsets.only(top: title == null ? 0.0 : 10.0),
        child: const CupertinoActivityIndicator(),
      ),
    );
  }
}

class MaterialLoadingDialog extends StatelessWidget {
  final String? title;

  const MaterialLoadingDialog({this.title, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: title == null ? null : Text(title!),
      content: Column(mainAxisSize: MainAxisSize.min, children: const [
        Padding(
          padding: EdgeInsets.only(top: 10.0),
          child: CircularProgressIndicator(),
        )
      ]),
    );
  }
}
