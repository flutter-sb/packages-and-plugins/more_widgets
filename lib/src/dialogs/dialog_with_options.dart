/// Copyright 2023, simonbraillard
/// simonbraillard - more_widgets
///
/// File : dialog_with_options.dart
/// Author : simonbraillard
/// Date : 10.03.23

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:more_widgets/more_widgets.dart';

class CupertinoDialogWithOptions extends StatelessWidget {
  final String title;
  final String message;

  final String textLeftButton;
  final String textRightButton;
  final Function? onPressedLeftButton;
  final Function? onPressedRightButton;

  final DestructiveAction destructiveAction;
  final DefaultAction defaultAction;
  final bool popByDefault;

  const CupertinoDialogWithOptions(
      {required this.title,
      required this.message,
      required this.popByDefault,
      required this.destructiveAction,
      required this.defaultAction,
      required this.textLeftButton,
      required this.textRightButton,
      this.onPressedLeftButton,
      this.onPressedRightButton,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        CupertinoDialogAction(
          isDestructiveAction:
              destructiveAction == DestructiveAction.left ? true : false,
          isDefaultAction: defaultAction == DefaultAction.left ? true : false,
          onPressed: () {
            if (popByDefault) Navigator.of(context).pop();
            onPressedLeftButton?.call();
          },
          child: Text(textLeftButton),
        ),
        CupertinoDialogAction(
          isDestructiveAction:
              destructiveAction == DestructiveAction.right ? true : false,
          isDefaultAction: defaultAction == DefaultAction.right ? true : false,
          onPressed: () {
            if (popByDefault) Navigator.of(context).pop();
            onPressedRightButton?.call();
          },
          child: Text(textRightButton),
        ),
      ],
    );
  }
}

class MaterialDialogWithOptions extends StatelessWidget {
  final String title;
  final String message;
  final String textLeftButton;

  /// You can change the dismiss button text
  final String textRightButton;
  final Function? onPressedLeftButton;
  final Function? onPressedRightButton;

  /// If set to true, the right button will be considered as a destructive button and will be colored in red
  final bool popByDefault;
  final DestructiveAction destructiveAction;
  final DefaultAction defaultAction;
  final Color androidDestructiveColor;

  const MaterialDialogWithOptions(
      {required this.title,
      required this.message,
      required this.popByDefault,
      required this.destructiveAction,
      required this.defaultAction,
      required this.textLeftButton,
      required this.textRightButton,
      required this.androidDestructiveColor,
      this.onPressedLeftButton,
      this.onPressedRightButton,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        TextButton(
          onPressed: () {
            if (popByDefault) Navigator.of(context).pop();
            onPressedLeftButton?.call();
          },
          child: Text(textLeftButton,
              style: TextStyle(
                color: destructiveAction == DestructiveAction.left
                    ? androidDestructiveColor
                    : null,
                fontWeight: defaultAction == DefaultAction.left
                    ? FontWeight.bold
                    : null,
              )),
        ),
        TextButton(
          onPressed: () {
            if (popByDefault) Navigator.of(context).pop();
            onPressedRightButton?.call();
          },
          child: Text(textRightButton,
              style: TextStyle(
                color: destructiveAction == DestructiveAction.right
                    ? androidDestructiveColor
                    : null,
                fontWeight: defaultAction == DefaultAction.right
                    ? FontWeight.bold
                    : null,
              )),
        ),
      ],
    );
  }
}
