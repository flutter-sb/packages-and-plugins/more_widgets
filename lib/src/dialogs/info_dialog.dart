/// Copyright 2023, simonbraillard
/// simonbraillard - more_widgets
///
/// File : info_dialog.dart
/// Author : simonbraillard
/// Date : 10.03.23

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CupertinoInfoDialog extends StatelessWidget {
  final String title;
  final String buttonText;
  final String message;
  final bool popByDefault;
  final Function? onPressed;

  const CupertinoInfoDialog(
      {required this.title,
      required this.message,
      required this.buttonText,
      required this.popByDefault,
      this.onPressed,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        CupertinoDialogAction(
          child: Text(buttonText, textAlign: TextAlign.justify),
          onPressed: () {
            if (popByDefault) {
              Navigator.of(context).pop();
            }
            onPressed?.call();
          },
        ),
      ],
    );
  }
}

class MaterialInfoDialog extends StatelessWidget {
  final String title;
  final String buttonText;
  final String message;
  final bool popByDefault;
  final Function? onPressed;

  const MaterialInfoDialog(
      {required this.title,
      required this.message,
      required this.buttonText,
      required this.popByDefault,
      this.onPressed,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: [
        TextButton(
          child: Text(buttonText, textAlign: TextAlign.justify),
          onPressed: () {
            if (popByDefault) {
              Navigator.of(context).pop();
            }
            onPressed?.call();
          },
        ),
      ],
    );
  }
}
