## 3.1.6 [13 june 2023]

* Adding barrierDismissible to dialogs
* Dart 3 ready

## 3.1.5 [17 april 2023]

* Turning dialogs into futures

## 3.1.4 [10 mars 2023]

* Fixing readme issues

## 3.1.3 [10 mars 2023]

* Fixing readme issues

## 3.1.2 [10 mars 2023]

* Improving Readme with pictures

## 3.1.1 [10 mars 2023]

* Improving structure
* Dialogs refactoring
* Example app is now runnable

## 3.1.0 [08 mars 2022]

* Removing all of the auth screen
* Min dart sdk version is now 2.19.2
* Min flutter version is now 3.0.0
* Upgrading dependencies

## 3.0.6 [18 mars 2021]

* Upgrading dependencies

## 3.0.5 [26 december 2021]

* Bug fix

## 3.0.4

* Adding a loading widget

## 3.0.3

* Possibility to display number keyboard on textInputDialog

## 3.0.2

* Formatting code to flutter format

## 3.0.1

New :
* Added loading dialogs
* Added textInputDialog
* Added dialogWithOptions
* Added example

## 3.0.0
Restarting this package from start. **Don't upgrade** if you used some wigets that are not in the
package anymore. They'll be back in the future.

* Adding documentations for widgets
* Start merging package `MyEasyDialog` in this one.
* Updating to latest flutter version

### Widgets available :
* RoundedContainer()
* GradientBackground()
* Dialogs.infodialog()

## 2.0.14

* Bug fix with brightness

## 2.0.13

* Bug fix with brightness

## 2.0.12

* Bug fix

## 2.0.11

* Bug fix

## 2.0.10

* Changing gradientButton to make it available in dark mode !

## 2.0.9

* Bug fix

## 2.0.8

* Bug fix

## 2.0.7

* Bug fix

## 2.0.6

* adding animated overlay
  * AnimatedValidateOverlay
  * AnimatedIconOverlay

## 2.0.5

* possibility to hide underline formfield

## 2.0.4

* possibility to change icons from text field

## 2.0.3

* Added possibility to readOnly text fields

## 2.0.2

* bug fix

## 2.0.1

* New ```Names form field```

## 2.0.0

* First version
