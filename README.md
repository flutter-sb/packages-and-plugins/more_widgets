# more_widgets
Add more widgets to use in your apps...

## List of widgets :
### GradientBackground :
Shows a gradient background container. Here is the parameters list:
```dart
final Widget child;
final List<Color> colors;       // Default : const [Color(0xffff9800), Color(0xfff44336)]
final List<Color> darkColors;   // Default : const [Color(0xff13191f), Color(0xff262f3c)]
final List<double> stops;       // Default : const [0.2, 0.8]
final AlignmentGeometry begin;  // Default : FractionalOffset.topLeft
final AlignmentGeometry end;    // Default : FractionalOffset.bottomRight
final bool useDarkMode;         // Default : true
```


### RoundedContainer :
Shows a rounded container. The default color is white. Here is the parameters list:
```dart
final Widget? child;
final Color color;            // Default : Colors.white
final double circularRadius;  // Default : 20
final double margin;          // Default : 20
final double padding;         // Default : 20
```

## List of dialogs :
Those dialogs adapt automatically to the OS and display accordingly.

### Dialogs.infoDialog
This shows an informative dialog with only one button. Here is the parameters list:

```dart
required BuildContext context,
required String title,
required String message,
String buttonText = "Ok",
bool popByDefault = true,
Function? onPressed,
```

| iOS                                | Android (with material design 3)                                              |
|------------------------------------|-------------------------------------------------------------------------------|
| ![iOS](https://drive.google.com/uc?id=1-5_HoVvr8op5Rht8RAtKcxqOyfqNVVfh) | ![Android](https://drive.google.com/uc?id=1y47tyW-Cb0leaGdWll8hWceiX3m6lHn-) |

### Dialogs.dialogWithOptions
Here is the dialog with options, it's useful if you want to pop up a dialog where you can choose between 
two options. One of them can be set as destructive action. Here is the parameters list:

```dart
required BuildContext context,
required String title,
required String message,
String textLeftButton = 'OK',
String textRightButton = 'Cancel',
Function? onPressedLeftButton,
Function? onPressedRightButton,
DestructiveAction destructiveAction = DestructiveAction.right,
DefaultAction defaultAction = DefaultAction.none,
Color androidDestructiveColor = Colors.red,
bool popByDefault = true,
```


| iOS                                        | Android (with material design 3)                   |
|--------------------------------------------|----------------------------------------------------|
| ![iOS](https://drive.google.com/uc?id=1zchbjdNPpeOa3SYV5vB9bLIjpzHa6CiN) | ![Android](https://drive.google.com/uc?id=1DswLrL9W4VgfqQf-H-ps52Uqs42aWJqD) |

### Dialogs.textInputDialog
This method will display a dialog with one or two buttons that have actions. Here is the parameters list:

```dart
required BuildContext context,
required String title,
required String message,
String textLeftButton = 'OK',
String textRightButton = 'Cancel',
Function? onPressedLeftButton,
Function? onPressedRightButton,
DestructiveAction destructiveAction = DestructiveAction.right,
DefaultAction defaultAction = DefaultAction.none,
Color androidDestructiveColor = Colors.red,
bool popByDefault = true,
bool hasSecondaryButton = true,
String? placeholder,
TextEditingController? controller,
TextInputType? keyboardType,
Function? onChanged,
Function? onEditingComplete,
```
| iOS                                                                          | Android (with material design 3)                 |
|------------------------------------------------------------------------------|--------------------------------------------------|
| ![iOS](https://drive.google.com/uc?id=1681fi-Lx__bo-_NnLnRnQQO8lW9Xny8h) | ![Android](https://drive.google.com/uc?id=1i1sKnFGXuanRN1s9luyzZgLfMoCK0S1d) |

### Dialogs.loadingDialog

```dart
required BuildContext context,
String? title,
```
| iOS                                   | Android (with material design 3)              |
|---------------------------------------|-----------------------------------------------|
| ![iOS](https://drive.google.com/uc?id=1D8ZNPWmJj7dWx6QqGKLEm2Pu81eRj7Z6) | ![Android](https://drive.google.com/uc?id=1QqSUjXyldqnTAagpc7LlzH5euwP5JD1R) |

## Left to do
- [ ] Adapt dialogs design to MacOS
- [ ] Adapt dialogs design to Windows
- [ ] Adapt dialogs design to Linux
- [ ] Improve code documentation